var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var fs = require('fs');

var config = require('../myConfig.js');
/* GET home page. */
router.get('/', function (req, res, next) {
   var return_data = {};

   var pool = mysql.createPool(config.database);

   pool.getConnection((err, connection) => {
      if (err) throw err;
      connection.query('SELECT id, year, reclat, reclong FROM landing2 LIMIT 50', function (error, rows, fields) {
         if (error) {
            console.log(error);
            connection.release();
         } else {
            connection.release();
            res.render('index', {
               title: 'mimpact',
               data: JSON.stringify(rows),
            });
         }

      
   });
   });
});

module.exports = router;